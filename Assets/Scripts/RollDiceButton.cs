
using UnityEngine;

public class RollDiceButton : UnityEngine.MonoBehaviour
{
    public GameObject Player;
    private int currentTile = 1;
    
        void OnMouseDown() {
            Play();
        }
        
        void Play()
        {
            var playerMover = Player.GetComponent<PlayerMovement>();
            currentTile += 4;
            playerMover.Tile = currentTile;
            UnityEngine.Debug.Log("Carajinetos");
        }
        
}
